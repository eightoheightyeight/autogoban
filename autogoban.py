import cv2
import numpy as np
from ConfigParser import SafeConfigParser

'''
TODO:
X - BASIC PIECE DETECTION & COLOR DETERMINATION
X - SAVE/LOAD CONFIG
CAMERA WHITE BALANCE ADJUST, ETC (AKA FIGURE OUT LIGHTING)
SET/DETECT GO BOARD TOP-LEFT/BOTTOM-RIGHT COORDINATES
GO BOARD STATE
SKEW OPTION/SLIDER TO CORRECT PERSPECTIVE
AUTO-CALIBRATE SETTINGS

'''

def ev(x):
    pass

def saturate(img, amount):
    simg = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    return cv2.cvtColor(simg, cv2.COLOR_HSV2BGR)

def detect(rawimg, cx1,cx2,cy1,cy2):
    aimg=rawimg[cy1:cy2, cx1:cx2]
    simg=saturate(aimg, 100)

    #img = cv2.cvtColor(img,cv2.COLOR_BGR)
    img = cv2.cvtColor(aimg,cv2.COLOR_BGR2GRAY)
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    #img = cv2.cvtColor(cv2.copyMakeBorder(aimg,0,0,0,0,cv2.BORDER_REPLICATE), )

    bigmask = cv2.compare(img,np.uint8([127]),cv2.CMP_GE)
    smallmask = cv2.bitwise_not(bigmask)

    x = cv2.getTrackbarPos('contrast','Goban')
    big = cv2.add(img,x,mask = bigmask)
    small = cv2.subtract(img,x,mask = smallmask)
    thresh = cv2.add(big,small)

    minD=cv2.getTrackbarPos('minDist','Goban')
    p1=cv2.getTrackbarPos('param1','Goban')
    p2=cv2.getTrackbarPos('param2','Goban')
    minR=cv2.getTrackbarPos('minRadius','Goban')
    maxR=cv2.getTrackbarPos('maxRadius','Goban')

    circles = cv2.HoughCircles(img,cv2.cv.CV_HOUGH_GRADIENT,1,minD,param1=p1,param2=p2,minRadius=minR,maxRadius=maxR)
    if not circles is None:
        for i in circles[0,:]:
            #color
            col = thresh[i[1], i[0]]
            boardcol = simg[i[1], i[0]]
            bmax = max(boardcol[0],boardcol[1],boardcol[2])
            bmin = min(boardcol[0],boardcol[1],boardcol[2])
            diff = bmax-bmin

            if diff > cv2.getTrackbarPos('colorThresh','Goban'):
                col = (0,0,255) #false pos
            elif col<127:
                col = (255,128,0) #BLACK stone
                cv2.circle(cimg,(i[0],i[1]),i[2],col,2)
            else:
                col = (0,255,0) #WHITE stone
                cv2.circle(cimg,(i[0],i[1]),i[2],col,2)

            # draw the center of the circle
            cv2.circle(cimg,(i[0],i[1]),2,col,3)
        cv2.imshow('Detected',cimg)

def saveConfig():
    config = SafeConfigParser()
    config.read('settings.ini')
    if not config.has_section('main'):
        config.add_section('main')
    config.set('main', 'minDist', str(cv2.getTrackbarPos('minDist', 'Goban')))
    config.set('main', 'param1', str(cv2.getTrackbarPos('param1', 'Goban')))
    config.set('main', 'param2', str(cv2.getTrackbarPos('param2', 'Goban')))
    config.set('main', 'minRadius', str(cv2.getTrackbarPos('minRadius', 'Goban')))
    config.set('main', 'maxRadius', str(cv2.getTrackbarPos('maxRadius', 'Goban')))
    config.set('main', 'contrast', str(cv2.getTrackbarPos('contrast', 'Goban')))
    config.set('main', 'crop_x1', str(cv2.getTrackbarPos('crop_x1', 'Goban')))
    config.set('main', 'crop_x2', str(cv2.getTrackbarPos('crop_x2', 'Goban')))
    config.set('main', 'crop_y1', str(cv2.getTrackbarPos('crop_y1', 'Goban')))
    config.set('main', 'crop_y2', str(cv2.getTrackbarPos('crop_y2', 'Goban')))
    config.set('main', 'colorThresh', str(cv2.getTrackbarPos('colorThresh', 'Goban')))

    with open('settings.ini', 'w') as f:
        config.write(f)


#***** MAIN *****
minDist=20
param1=60
param2=20
minRadius=10
maxRadius=25
contrast=70
crop_x1=0
crop_x2=1920
crop_y1=0
crop_y2=1080
colorThresh=50

config = SafeConfigParser()
configs = config.read('settings.ini')
if len(configs)>0:
    minDist=config.getint('main','minDist')
    param1=config.getint('main','param1')
    param2=config.getint('main','param2')
    minRadius=config.getint('main','minRadius')
    maxRadius=config.getint('main','maxRadius')
    contrast=config.getint('main','contrast')
    crop_x1=config.getint('main','crop_x1')
    crop_x2=config.getint('main','crop_x2')
    crop_y1=config.getint('main','crop_y1')
    crop_y2=config.getint('main','crop_y2')
    colorThresh=config.getint('main','colorThresh')

cv2.namedWindow('Goban')
cv2.createTrackbar('minDist','Goban',minDist,255,ev)
cv2.createTrackbar('param1','Goban',param1,255,ev)
cv2.createTrackbar('param2','Goban',param2,255,ev)
cv2.createTrackbar('minRadius','Goban',minRadius,255,ev)
cv2.createTrackbar('maxRadius','Goban',maxRadius,255,ev)
cv2.createTrackbar('contrast','Goban',contrast,255,ev)
cv2.createTrackbar('crop_x1','Goban',crop_x1,1920,ev)
cv2.createTrackbar('crop_y1','Goban',crop_y1,1080,ev)
cv2.createTrackbar('crop_x2','Goban',crop_x2,1920,ev)
cv2.createTrackbar('crop_y2','Goban',crop_y2,1080,ev)
cv2.createTrackbar('colorThresh','Goban',colorThresh,255,ev)


cap = cv2.VideoCapture(0)
if not cap.isOpened():
    print "error"
    exit()

cap.set(3, 1920)
cap.set(4, 1080)
print cap

#not saved
cv2.createTrackbar('brightness','Goban',int(cap.get(10)*100.0),100,ev)
cv2.createTrackbar('cam-contrast','Goban',int(cap.get(11)*100.0),100,ev)
cv2.createTrackbar('saturation','Goban',int(cap.get(12)*100.0),100,ev)
cv2.createTrackbar('hue','Goban',int(cap.get(13)*100.0),100,ev)
cv2.createTrackbar('gain','Goban',int(cap.get(14)*100.0),100,ev)
cv2.createTrackbar('exposure','Goban',int(cap.get(15)*100.0),100,ev)


while(1):
    cap.set(10, cv2.getTrackbarPos('brightness','Goban')/100.0)
    cap.set(11, cv2.getTrackbarPos('cam-contrast','Goban')/100.0)
    cap.set(12, cv2.getTrackbarPos('saturation','Goban')/100.0)
    cap.set(13, cv2.getTrackbarPos('hue','Goban')/100.0)
    cap.set(14, cv2.getTrackbarPos('gain','Goban')/100.0)
    cap.set(15, cv2.getTrackbarPos('exposure','Goban')/100.0)

    ret, img = cap.read()
    img = cv2.flip(img, 0)
    img = cv2.flip(img, 1)

    k = cv2.waitKey(50)
    if ret:
        cx1=cv2.getTrackbarPos('crop_x1','Goban')
        cy1=cv2.getTrackbarPos('crop_y1','Goban')
        cx2=cv2.getTrackbarPos('crop_x2','Goban')
        cy2=cv2.getTrackbarPos('crop_y2','Goban')

        #img=cv2.flip(img, 0)
        cv2.imshow('Camera',img[cy1:cy2, cx1:cx2])
        #img2 = cv2.medianBlur(img,1)
        detect(img,cx1,cx2,cy1,cy2)

        if not k==-1:
            saveConfig()

cv2.destroyAllWindows()
